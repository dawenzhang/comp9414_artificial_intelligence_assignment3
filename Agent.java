
/***********************************************************************************************************************************************************************
 *  Agent.java 
 *  Agent for Text-Based Adventure Game
 *  COMP3411/9414/9814 Artificial Intelligence
 *  UNSW Session 1, 2018
 *  Modified from Sample Program by Dawen Zhang
*/

/* Question:
 * 
 * Briefly describe how your program works, including any algorithms and data structures employed, and explain any design decisions you made along the way.
 */

/* Answer:
 * 
 * This program runs as Agent client that receives views from the game server and uses integrated information to send commands back to the game server in order to find treasure and go back to the initial location. The agent gets context information along with its prior commands and stores these into three maps: world map, explorable map and explored map. It will also predict and store the items it holds when the commands are being sent. When there are points that the agent can explore at the stage but have not explored, it navigates there, using seed floodfill algorithm.
 * 
 * When it have explored everywhere it can explore, and cannot get the treasure and back to initial location, it makes decisions. The agent tries to find the border of the water it can reach, "seaside", when it is on the land; or the border of the land it can reach, "coast", when it is in the water. After these choices collected, the agent recursively verifies them and their further choices. once a further choice reached the state of "standing on the initial point with treasure", it returns the decision leads to it as the first element of a new list. Its ealier decisions just add themselves at the rear of the returned list. If some choices reached a state of "not won and has no further choices" which might because of the unexplored places of the game, it returns the decision leads to it as the  add a NULL value at the first place. Its ealier decisions add themselves at the rear of the returned list of their longest further decisions list. The no-NULL-head decision lists can reach the win state following the decisions in the list while the NULL-head decision lists should try one decision and then get more information from context. The method can be decribed as follows.
 * 
 *                          one further decision returns [decision, ...]: return [decision, ..., current decision]
 *                          all further decisions return [NULL, decision, ...]: select longest and return [NULL, decision, ..., current decision]      
 *                                                            .
 *                                                            .                           /--- return [current decision]
 *                                                            .          /--- state: won /
 * Explore --- state: explored everywhere at the stage ---> Make Decision --- state: not won and can make further decisions ---\
 *    ^                                                       |   ^                                                             \
 *    |                                                       |   |                                                             /
 *    |                                                       |   \------------------------------------------------------------/
 *    |                                                       \
 *    |                                                        \--- state: not won and cannot make further decisions --- return [NULL, current decision]
 *    |                                                     {=======================================================================================}
 *    \                                                                                                /
 *     \------------------------------------------- root decision node returns ----------------------/
 * 
 * There are also some pruning in the decision making stage, these are more like graphic process problems:
 *     when have raft and currently on the land, some outbound points into the water may have same effects, therefore merge them;
 *     when on the raft, some inbound points onto the land may have same effects, therefore merge them;
 *     when only have stone and currently on the land, the total stones remaining to use cannot reach any unexplored land from some water points, therefore drop them;
 *     when some decision cannot achieve anything and no new further decisions compared with its ealier one, therefore drop it.
 * 
 * Data structures used: basic atom structures: int, bool, char, ... possibly with their array forms and Java util Objects: ArrayList, Hashmap;
 * Algorithms used: Seed FloodFill is the mostly used one.
 */

import java.util.*;
import java.lang.*;
import java.io.*;
import java.net.*;

// The Agent class handles the task
public class Agent {

    // world map drew as real world map
    char world_map[][];
    // explorable map specifies where is available to be explored
    char explorable_map[][];
    // explored map specifies where has been explored
    char explored_map[][];
    // record the current position
    Integer current_pos[];
    // record the direction: 0 for North, 1 for East, 2 for South, 3 for West
    Integer direction;
    // record the latest input to the server
    char last_input;
    // the moving steps towards particular position
    ArrayList<Integer[]> path;
    // the action serial to move the the target position
    ArrayList<Character> act_list;
    // list of decision tree nodes
    ArrayList<Integer[]> decisions;
    // boolean pointer indicates whether is on the raft
    boolean rafting[];
    // boolean pointer indicates whether is during a navigating between water and
    // land
    boolean navigating[];
    // hashmap indicates the tools
    HashMap<String, Integer> items;

    // initialization of agent
    public Agent() {

        // as the size of map is no more than 80 * 80, and there maybe 2 units of border
        // area marked as '.', to make it safe, the map size here is 163 * 163

        this.world_map = new char[163][163];
        for (int i = 0; i < world_map.length; i++) {
            for (int j = 0; j < this.world_map[i].length; j++) {
                this.world_map[i][j] = 'X';
            }
        }

        this.explorable_map = new char[163][163];
        for (int i = 0; i < explorable_map.length; i++) {
            for (int j = 0; j < this.explorable_map[i].length; j++) {
                this.explorable_map[i][j] = '#';
            }
        }

        this.explored_map = new char[163][163];
        for (int i = 0; i < explored_map.length; i++) {
            for (int j = 0; j < this.explored_map[i].length; j++) {
                this.explored_map[i][j] = '!';
            }
        }

        // the default spawn point is set to the center of the map : (81, 81)

        this.current_pos = new Integer[] { 81, 81 };

        this.world_map[this.current_pos[0]][this.current_pos[1]] = ' ';

        this.direction = 0;

        this.last_input = ' ';

        this.path = new ArrayList<Integer[]>();

        this.act_list = new ArrayList<Character>();

        this.decisions = new ArrayList<Integer[]>();

        this.rafting = new boolean[] { false };

        this.navigating = new boolean[] { false };

        this.items = new HashMap<String, Integer>();
        this.items.put("key", 0);
        this.items.put("axe", 0);
        this.items.put("raft", 0);
        this.items.put("stone", 0);
        this.items.put("treasure", 0);
    }

    // generate three maps from view and prior maps and records
    void get_world_map(char[][] view, char[][] world_map, char[][] explorable_map, char[][] explored_map,
            Integer[] current_pos, Integer direction, char last_input) {
        for (int i = 0; i < view.length; i++) {
            for (int j = 0; j < view[i].length; j++) {
                if (i == 2 && j == 2) {
                    explored_map[current_pos[0]][current_pos[1]] = '+';
                    explorable_map[current_pos[0]][current_pos[1]] = '@';
                    if (world_map[current_pos[0]][current_pos[1]] == 'O'
                            || world_map[current_pos[0]][current_pos[1]] == '~') {

                    } else {
                        world_map[current_pos[0]][current_pos[1]] = ' ';
                    }
                } else {
                    if (((i == 1 || i == 3) && j == 2) || (i == 2 && (j == 1 || j == 3))) {
                        if (view[i][j] == ' ' || view[i][j] == 'a' || view[i][j] == 'o' || view[i][j] == 'k'
                                || view[i][j] == '$') {
                            if (direction == 0) {
                                explorable_map[current_pos[0] - 2 + i][current_pos[1] - 2 + j] = '@';
                            }
                            if (direction == 1) {
                                explorable_map[current_pos[0] - 2 + j][current_pos[1] + 2 - i] = '@';
                            }
                            if (direction == 2) {
                                explorable_map[current_pos[0] + 2 - i][current_pos[1] + 2 - j] = '@';
                            }
                            if (direction == 3) {
                                explorable_map[current_pos[0] + 2 - j][current_pos[1] - 2 + i] = '@';
                            }
                        }
                        if (view[i][j] == 'o' && items.get("stone") <= 0) {
                            if (direction == 0) {
                                explorable_map[current_pos[0] - 2 + i][current_pos[1] - 2 + j] = '@';
                            }
                            if (direction == 1) {
                                explorable_map[current_pos[0] - 2 + j][current_pos[1] + 2 - i] = '@';
                            }
                            if (direction == 2) {
                                explorable_map[current_pos[0] + 2 - i][current_pos[1] + 2 - j] = '@';
                            }
                            if (direction == 3) {
                                explorable_map[current_pos[0] + 2 - j][current_pos[1] - 2 + i] = '@';
                            }
                        } else if (view[i][j] == 'o') {
                            if (direction == 0) {
                                explorable_map[current_pos[0] - 2 + i][current_pos[1] - 2 + j] = '&';
                            }
                            if (direction == 1) {
                                explorable_map[current_pos[0] - 2 + j][current_pos[1] + 2 - i] = '&';
                            }
                            if (direction == 2) {
                                explorable_map[current_pos[0] + 2 - i][current_pos[1] + 2 - j] = '&';
                            }
                            if (direction == 3) {
                                explorable_map[current_pos[0] + 2 - j][current_pos[1] - 2 + i] = '&';
                            }
                        }

                        if (view[i][j] == '~') {
                            if (direction == 0) {
                                explorable_map[current_pos[0] - 2 + i][current_pos[1] - 2 + j] = '~';
                            }
                            if (direction == 1) {
                                explorable_map[current_pos[0] - 2 + j][current_pos[1] + 2 - i] = '~';
                            }
                            if (direction == 2) {
                                explorable_map[current_pos[0] + 2 - i][current_pos[1] + 2 - j] = '~';
                            }
                            if (direction == 3) {
                                explorable_map[current_pos[0] + 2 - j][current_pos[1] - 2 + i] = '~';
                            }
                        }
                    }
                    if (view[i][j] == 'T' && items.get("raft") <= 0) {
                        if (direction == 0) {
                            explorable_map[current_pos[0] - 2 + i][current_pos[1] - 2 + j] = view[i][j];
                        }
                        if (direction == 1) {
                            explorable_map[current_pos[0] - 2 + j][current_pos[1] + 2 - i] = view[i][j];
                        }
                        if (direction == 2) {
                            explorable_map[current_pos[0] + 2 - i][current_pos[1] + 2 - j] = view[i][j];
                        }
                        if (direction == 3) {
                            explorable_map[current_pos[0] + 2 - j][current_pos[1] - 2 + i] = view[i][j];
                        }
                    } else if (view[i][j] == 'T') {
                        if (direction == 0) {
                            explorable_map[current_pos[0] - 2 + i][current_pos[1] - 2 + j] = '(';
                        }
                        if (direction == 1) {
                            explorable_map[current_pos[0] - 2 + j][current_pos[1] + 2 - i] = '(';
                        }
                        if (direction == 2) {
                            explorable_map[current_pos[0] + 2 - i][current_pos[1] + 2 - j] = '(';
                        }
                        if (direction == 3) {
                            explorable_map[current_pos[0] + 2 - j][current_pos[1] - 2 + i] = '(';
                        }
                    }

                    if (view[i][j] == '-') {
                        if (direction == 0) {
                            explorable_map[current_pos[0] - 2 + i][current_pos[1] - 2 + j] = view[i][j];
                        }
                        if (direction == 1) {
                            explorable_map[current_pos[0] - 2 + j][current_pos[1] + 2 - i] = view[i][j];
                        }
                        if (direction == 2) {
                            explorable_map[current_pos[0] + 2 - i][current_pos[1] + 2 - j] = view[i][j];
                        }
                        if (direction == 3) {
                            explorable_map[current_pos[0] + 2 - j][current_pos[1] - 2 + i] = view[i][j];
                        }
                    }

                    if (direction == 0) {
                        world_map[current_pos[0] - 2 + i][current_pos[1] - 2 + j] = view[i][j];
                    }
                    if (direction == 1) {
                        world_map[current_pos[0] - 2 + j][current_pos[1] + 2 - i] = view[i][j];
                    }
                    if (direction == 2) {
                        world_map[current_pos[0] + 2 - i][current_pos[1] + 2 - j] = view[i][j];
                    }
                    if (direction == 3) {
                        world_map[current_pos[0] + 2 - j][current_pos[1] - 2 + i] = view[i][j];
                    }
                }
            }
        }
    }

    // in some very very special situation, the stone cannot be picked before the
    // raft has been used

    void refresh_stones(char[][] explorable_map, HashMap<String, Integer> items) {
        for (int i = 0; i < explorable_map.length; i++) {
            for (int j = 0; j < explorable_map[i].length; j++) {
                if (explorable_map[i][j] == '&' && items.get("stone") <= 0) {
                    explorable_map[i][j] = '@';
                }
            }
        }
    }

    void enable_more_stone(char[][] explorable_map, HashMap<String, Integer> items) {
        for (int i = 0; i < explorable_map.length; i++) {
            for (int j = 0; j < explorable_map[i].length; j++) {
                if (explorable_map[i][j] == '&' && items.get("stone") == 1 && items.get("raft") <= 0) {
                    explorable_map[i][j] = '@';
                    return;
                }
            }
        }
    }

    // generate tools from view and prior knowledge
    void get_items(HashMap<String, Integer> items, char[][] view, char[][] world_map, Integer[] current_pos,
            Integer direction, char last_input) {
        if (last_input == 'f' || last_input == 'F') {
            if (view[1][2] == '$') {
                items.replace("treasure", items.get("treasure") + 1);
            }

            if (view[1][2] == 'k') {
                items.replace("key", items.get("key") + 1);
            }

            if (view[1][2] == 'a') {
                items.replace("axe", items.get("axe") + 1);
            }
            if (view[1][2] == 'o') {
                items.replace("stone", items.get("stone") + 1);
            }

            if (view[1][2] == '~') {
                if (items.get("stone") > 0) {
                    items.replace("stone", items.get("stone") - 1);
                    if (direction == 0) {
                        world_map[current_pos[0] - 1][current_pos[1]] = 'O';
                    }
                    if (direction == 1) {
                        world_map[current_pos[0]][current_pos[1] + 1] = 'O';
                    }
                    if (direction == 2) {
                        world_map[current_pos[0] + 1][current_pos[1]] = 'O';
                    }
                    if (direction == 3) {
                        world_map[current_pos[0]][current_pos[1] - 1] = 'O';
                    }
                } else if (items.get("raft") > 0) {
                    items.replace("raft", items.get("raft") - 1);
                }
            }

            if (view[1][2] == '$' || view[1][2] == 'k' || view[1][2] == 'a' || view[1][2] == 'o') {
                if (direction == 0) {
                    world_map[current_pos[0] - 1][current_pos[1]] = ' ';
                }
                if (direction == 1) {
                    world_map[current_pos[0]][current_pos[1] + 1] = ' ';
                }
                if (direction == 2) {
                    world_map[current_pos[0] + 1][current_pos[1]] = ' ';
                }
                if (direction == 3) {
                    world_map[current_pos[0]][current_pos[1] - 1] = ' ';
                }
            }
        }
        if (last_input == 'c' || last_input == 'C') {
            if (view[1][2] == 'T' && items.get("axe") > 0) {
                items.replace("raft", items.get("raft") + 1);
                if (direction == 0) {
                    world_map[current_pos[0] - 1][current_pos[1]] = ' ';
                }
                if (direction == 1) {
                    world_map[current_pos[0]][current_pos[1] + 1] = ' ';
                }
                if (direction == 2) {
                    world_map[current_pos[0] + 1][current_pos[1]] = ' ';
                }
                if (direction == 3) {
                    world_map[current_pos[0]][current_pos[1] - 1] = ' ';
                }
            }
        }
    }

    // calculate the direction facing
    Integer get_direction(int direction, char last_input) {
        if (last_input == ' ' || last_input == 'f' || last_input == 'F' || last_input == 'c' || last_input == 'C'
                || last_input == 'u' || last_input == 'U') {
            return direction;
        }
        if (last_input == 'l' || last_input == 'L') {
            return (direction + 3) % 4;
        }
        if (last_input == 'r' || last_input == 'R') {
            return (direction + 1) % 4;
        }
        return direction;
    }

    // calculate the coordinates
    Integer[] get_pos(Integer[] current_pos, Integer direction, char last_input) {
        if (last_input == ' ' || last_input == 'c' || last_input == 'C' || last_input == 'u' || last_input == 'U') {
            return current_pos;
        }
        if (last_input == 'l' || last_input == 'L') {
            return current_pos;
        }
        if (last_input == 'r' || last_input == 'R') {
            return current_pos;
        }
        if (last_input == 'f' || last_input == 'F') {
            if (direction == 0) {
                return new Integer[] { current_pos[0] - 1, current_pos[1] };
            }
            if (direction == 1) {
                return new Integer[] { current_pos[0], current_pos[1] + 1 };
            }
            if (direction == 2) {
                return new Integer[] { current_pos[0] + 1, current_pos[1] };
            }
            if (direction == 3) {
                return new Integer[] { current_pos[0], current_pos[1] - 1 };
            }
        }
        return current_pos;
    }

    // when making decision, this method simulate the moves of agent
    void simulate_move(char[][] world_map, char[][] explorable_map, char[][] explored_map, Integer[] moving_pos,
            HashMap<String, Integer> items) {
        explored_map[moving_pos[0]][moving_pos[1]] = '+';
        explorable_map[moving_pos[0]][moving_pos[1]] = '@';

        if (world_map[moving_pos[0]][moving_pos[1]] == '$') {
            items.replace("treasure", items.get("treasure") + 1);
        }

        if (world_map[moving_pos[0]][moving_pos[1]] == 'k') {
            items.replace("key", items.get("key") + 1);
        }

        if (world_map[moving_pos[0]][moving_pos[1]] == 'a') {
            items.replace("axe", items.get("axe") + 1);
        }

        if (world_map[moving_pos[0]][moving_pos[1]] == 'o') {
            items.replace("stone", items.get("stone") + 1);
        }

        if (world_map[moving_pos[0]][moving_pos[1]] == '~') {
            if (items.get("stone") > 0) {
                items.replace("stone", items.get("stone") - 1);
                world_map[moving_pos[0]][moving_pos[1]] = 'O';
            } else if (items.get("raft") > 0) {
                items.replace("raft", items.get("raft") - 1);
            }
        }

        if (world_map[moving_pos[0]][moving_pos[1]] == '$' || world_map[moving_pos[0]][moving_pos[1]] == 'k'
                || world_map[moving_pos[0]][moving_pos[1]] == 'a' || world_map[moving_pos[0]][moving_pos[1]] == 'o') {
            world_map[moving_pos[0]][moving_pos[1]] = ' ';
        }

        if (world_map[moving_pos[0]][moving_pos[1]] == 'T' && items.get("axe") > 0) {
            items.replace("raft", items.get("raft") + 1);
            world_map[moving_pos[0]][moving_pos[1]] = ' ';
        }

        if (world_map[moving_pos[0]][moving_pos[1]] == '-' && items.get("key") > 0) {
            world_map[moving_pos[0]][moving_pos[1]] = ' ';
        }

        if (world_map[moving_pos[0] + 1][moving_pos[1]] == '$' || world_map[moving_pos[0] + 1][moving_pos[1]] == ' '
                || world_map[moving_pos[0] + 1][moving_pos[1]] == 'a'
                || world_map[moving_pos[0] + 1][moving_pos[1]] == 'O'
                || world_map[moving_pos[0] + 1][moving_pos[1]] == 'k') {
            explorable_map[moving_pos[0] + 1][moving_pos[1]] = '@';
        }

        if (world_map[moving_pos[0] + 1][moving_pos[1]] == 'o' && items.get("stone") <= 0) {
            explorable_map[moving_pos[0] + 1][moving_pos[1]] = '@';
        } else if (world_map[moving_pos[0] + 1][moving_pos[1]] == 'o') {
            explorable_map[moving_pos[0] + 1][moving_pos[1]] = '&';
        }

        if (world_map[moving_pos[0] + 1][moving_pos[1]] == '~') {
            explorable_map[moving_pos[0] + 1][moving_pos[1]] = '~';
        }

        if (world_map[moving_pos[0] + 1][moving_pos[1]] == 'T' || world_map[moving_pos[0] + 1][moving_pos[1]] == '-') {
            explorable_map[moving_pos[0] + 1][moving_pos[1]] = world_map[moving_pos[0] + 1][moving_pos[1]];
        }

        if (world_map[moving_pos[0] - 1][moving_pos[1]] == '$' || world_map[moving_pos[0] - 1][moving_pos[1]] == ' '
                || world_map[moving_pos[0] - 1][moving_pos[1]] == 'a'
                || world_map[moving_pos[0] - 1][moving_pos[1]] == 'O'
                || world_map[moving_pos[0] - 1][moving_pos[1]] == 'k') {
            explorable_map[moving_pos[0] - 1][moving_pos[1]] = '@';
        }

        if (world_map[moving_pos[0] - 1][moving_pos[1]] == 'o' && items.get("stone") <= 0) {
            explorable_map[moving_pos[0] - 1][moving_pos[1]] = '@';
        } else if (world_map[moving_pos[0] - 1][moving_pos[1]] == 'o') {
            explorable_map[moving_pos[0] - 1][moving_pos[1]] = '&';
        }

        if (world_map[moving_pos[0] - 1][moving_pos[1]] == '~') {
            explorable_map[moving_pos[0] - 1][moving_pos[1]] = '~';
        }

        if (world_map[moving_pos[0] - 1][moving_pos[1]] == 'T' || world_map[moving_pos[0] - 1][moving_pos[1]] == '-') {
            explorable_map[moving_pos[0] - 1][moving_pos[1]] = world_map[moving_pos[0] - 1][moving_pos[1]];
        }

        if (world_map[moving_pos[0]][moving_pos[1] - 1] == '$' || world_map[moving_pos[0]][moving_pos[1] - 1] == ' '
                || world_map[moving_pos[0]][moving_pos[1] - 1] == 'a'
                || world_map[moving_pos[0]][moving_pos[1] - 1] == 'O'
                || world_map[moving_pos[0]][moving_pos[1] - 1] == 'k') {
            explorable_map[moving_pos[0]][moving_pos[1] - 1] = '@';
        }

        if (world_map[moving_pos[0]][moving_pos[1] - 1] == 'o' && items.get("stone") <= 0) {
            explorable_map[moving_pos[0]][moving_pos[1] - 1] = '@';
        } else if (world_map[moving_pos[0]][moving_pos[1] - 1] == 'o') {
            explorable_map[moving_pos[0]][moving_pos[1] - 1] = '&';
        }

        if (world_map[moving_pos[0]][moving_pos[1] - 1] == '~') {
            explorable_map[moving_pos[0]][moving_pos[1] - 1] = '~';
        }

        if (world_map[moving_pos[0]][moving_pos[1] - 1] == 'T' || world_map[moving_pos[0]][moving_pos[1] - 1] == '-') {
            explorable_map[moving_pos[0]][moving_pos[1] - 1] = world_map[moving_pos[0]][moving_pos[1] - 1];
        }

        if (world_map[moving_pos[0]][moving_pos[1] + 1] == '$' || world_map[moving_pos[0]][moving_pos[1] + 1] == ' '
                || world_map[moving_pos[0]][moving_pos[1] + 1] == 'a'
                || world_map[moving_pos[0]][moving_pos[1] + 1] == 'O'
                || world_map[moving_pos[0]][moving_pos[1] + 1] == 'k') {
            explorable_map[moving_pos[0]][moving_pos[1] + 1] = '@';
        }

        if (world_map[moving_pos[0]][moving_pos[1] + 1] == 'o' && items.get("stone") <= 0) {
            explorable_map[moving_pos[0]][moving_pos[1] + 1] = '@';
        } else if (world_map[moving_pos[0]][moving_pos[1] + 1] == 'o') {
            explorable_map[moving_pos[0]][moving_pos[1] + 1] = '&';
        }

        if (world_map[moving_pos[0]][moving_pos[1] + 1] == '~') {
            explorable_map[moving_pos[0]][moving_pos[1] + 1] = '~';
        }

        if (world_map[moving_pos[0]][moving_pos[1] + 1] == 'T' || world_map[moving_pos[0]][moving_pos[1] + 1] == '-') {
            explorable_map[moving_pos[0]][moving_pos[1] + 1] = world_map[moving_pos[0]][moving_pos[1] + 1];
        }
    }

    // making decision tree
    ArrayList<Integer[]> make_decision(ArrayList<Integer[]> former_choices, HashMap<String, Integer> former_items,
            Integer[] current_pos, char[][] world_map, char[][] explorable_map, char[][] explored_map,
            HashMap<String, Integer> items, boolean[] rafting, boolean[] navigating) {
        ArrayList<Integer[]> decision_nodes = new ArrayList<Integer[]>();
        ArrayList<ArrayList<Integer[]>> decision_collection = new ArrayList<ArrayList<Integer[]>>();

        Integer[] ddd = null;
        if (rafting[0] == true) {
            // find some inbound points for sea -> land
            ArrayList<Integer[]> coast = this.find_coast(current_pos, world_map, items);
            // prune some nodes: when using a raft, various 'ports' may have completely same
            // effect
            if (items.get("stone") <= 0 && items.get("raft") > 0) {
                ArrayList<Integer> merge_list = new ArrayList<Integer>();
                for (int coast_point = 0; coast_point < coast.size(); coast_point++) {
                    for (int coast_checker = 0; coast_checker < coast_point; coast_checker++) {
                        if (merge_coast_choices(coast.get(coast_checker), coast.get(coast_point), world_map) == true) {
                            if (merge_list.contains(coast_point) == false) {
                                merge_list.add(coast_point);
                            }
                        }
                    }
                }
                Collections.sort(merge_list);
                Collections.reverse(merge_list);
                for (int m = 0; m < merge_list.size(); m++) {
                    coast.remove(merge_list.get(m));
                }
            }
            for (int c = 0; c < coast.size(); c++) {
                decision_nodes.add(coast.get(c));
            }
        } else {
            // find some outbound points for land -> sea
            ArrayList<Integer[]> seaside = this.find_seaside(current_pos, world_map, items);
            // prune some nodes: to use a raft, various 'outbound points' may have
            // completely same effect
            if (items.get("stone") <= 0 && items.get("raft") > 0) {
                ArrayList<Integer> merge_list = new ArrayList<Integer>();
                for (int seaside_point = 0; seaside_point < seaside.size(); seaside_point++) {
                    for (int seaside_checker = 0; seaside_checker < seaside_point; seaside_checker++) {
                        if (merge_seaside_choices(seaside.get(seaside_checker), seaside.get(seaside_point),
                                world_map) == true) {
                            if (merge_list.contains(seaside_point) == false) {
                                merge_list.add(seaside_point);
                            }
                        }
                    }
                }
                Collections.sort(merge_list);
                Collections.reverse(merge_list);
                for (int m = 0; m < merge_list.size(); m++) {
                    seaside.remove(merge_list.get(m));
                }
            }
            for (int c = 0; c < seaside.size(); c++) {
                decision_nodes.add(seaside.get(c));
            }
        }

        // very special situation considering the order of stone\raft use
        for (int i = 0; i < world_map.length; i++) {
            for (int j = 0; j < world_map[i].length; j++) {
                if (items.get("stone") > 0) {
                    if (explorable_map[i][j] == '&' && world_map[i][j] == 'o'
                            && ((world_map[i + 1][j] != '*' && explorable_map[i + 1][j] == '#')
                                    || (world_map[i - 1][j] != '*' && explorable_map[i - 1][j] == '#')
                                    || (world_map[i][j + 1] != '*' && explorable_map[i][j + 1] == '#')
                                    || (world_map[i][j - 1] != '*' && explorable_map[i][j - 1] == '#'))) {
                        decision_nodes.add(new Integer[] { i, j });
                    }
                }
            }
        }

        if (decision_nodes.size() <= 0) {
            ArrayList<Integer[]> r = new ArrayList<Integer[]>();
            r.add(null);
            return r;
        }

        // prune some nodes: when the current decision cost tools and even cannot append
        // new decisions, it will be a bad decision
        boolean allin = true;
        for (Integer[] d : decision_nodes) {
            boolean inin = false;
            for (Integer[] dd : former_choices) {
                if (d[0] == dd[0] && d[1] == dd[1]) {
                    inin = true;
                    break;
                }
            }
            if (inin == false) {
                allin = false;
                break;
            }
        }

        boolean alless = false;
        boolean lessless = false;
        boolean moreq = false;
        for (String itkey : items.keySet()) {
            if (items.get(itkey) < former_items.get(itkey)) {
                lessless = true;
            }
            if (items.get(itkey) > former_items.get(itkey)) {
                moreq = true;
            }
        }

        if (moreq == false && lessless == true) {
            alless = true;
        }

        if (allin == true && alless == true) {
            ArrayList<Integer[]> r = new ArrayList<Integer[]>();
            r.add(null);
            return r;
        }

        Integer stone_count = items.get("stone");

        for (int exi = 0; exi < explorable_map.length; exi++) {
            for (int exj = 0; exj < explorable_map[exi].length; exj++) {
                if (explorable_map[exi][exj] == '&') {
                    stone_count += 1;
                }
            }
        }

        // prune some nodes: when only have stones, the outbound points only surrounded
        // by water and can never reach land with stones remaining are bad outbound
        // points
        if (world_map[current_pos[0]][current_pos[1]] == ' ' && items.get("stone") > 0 && items.get("raft") <= 0) {
            decision_nodes = this.remain_useful_water(decision_nodes, world_map, explored_map);
        }

        // generate new pointers for search
        for (Integer[] d : decision_nodes) {
            char w[][] = new char[163][163];
            for (int i = 0; i < w.length; i++) {
                for (int j = 0; j < w[i].length; j++) {
                    w[i][j] = world_map[i][j];
                }
            }

            char ea[][] = new char[163][163];
            for (int i = 0; i < ea.length; i++) {
                for (int j = 0; j < ea[i].length; j++) {
                    ea[i][j] = explorable_map[i][j];
                }
            }

            char ed[][] = new char[163][163];
            for (int i = 0; i < ed.length; i++) {
                for (int j = 0; j < ed[i].length; j++) {
                    ed[i][j] = explored_map[i][j];
                }
            }

            HashMap<String, Integer> it = new HashMap<String, Integer>();
            for (String itkey : items.keySet()) {
                it.put(itkey, items.get(itkey));
            }

            boolean[] raf = new boolean[1];
            raf[0] = rafting[0];

            boolean[] nav = new boolean[1];
            nav[0] = navigating[0];

            Integer[] cp = new Integer[] { current_pos[0], current_pos[1] };

            ea[d[0]][d[1]] = '@';
            ed[d[0]][d[1]] = '+';

            if (raf[0] == false && it.get("stone") <= 0 && w[d[0]][d[1]] == '~') {
                raf[0] = true;
                for (int i = 0; i < ea.length; i++) {
                    for (int j = 0; j < ea[i].length; j++) {
                        ea[i][j] = '#';
                    }
                }
                for (int i = 0; i < ed.length; i++) {
                    for (int j = 0; j < ed[i].length; j++) {
                        ed[i][j] = '!';
                    }
                }
                ea[d[0]][d[1]] = '@';
                ed[d[0]][d[1]] = '+';
            }

            if (raf[0] == true && w[d[0]][d[1]] != '~') {
                raf[0] = false;
                nav[0] = false;
                for (int i = 0; i < ea.length; i++) {
                    for (int j = 0; j < ea[i].length; j++) {
                        ea[i][j] = '#';
                    }
                }
                for (int i = 0; i < ed.length; i++) {
                    for (int j = 0; j < ed[i].length; j++) {
                        ed[i][j] = '!';
                    }
                }
                ea[d[0]][d[1]] = '@';
                ed[d[0]][d[1]] = '+';
            }

            // simulate the move of this dicision
            this.simulate_move(w, ea, ed, d, it);

            // after simulate the decision, simulate moving around to find tools
            ArrayList<Integer[]> further_path = this.explore(d, ea, ed, it, raf, nav);

            // when treasure obtained, mark spawn point as unexplored to navigate the agent
            // back
            if (it.get("treasure") > 0) {
                ed[81][81] = '!';
            }

            // when gone back with treasure on hold, it indicates good decisions, return
            if (cp[0] == 81 && cp[1] == 81 && it.get("treasure") > 0) {
                ArrayList<Integer[]> r = new ArrayList<Integer[]>();
                r.add(d);
                return r;
            }

            // continuous simulating exploring after decision simulating
            while (further_path.size() > 0) {
                while (further_path.size() > 0) {
                    Integer[] p = further_path.remove(0);
                    this.simulate_move(w, ea, ed, p, it);
                    cp = new Integer[] { p[0], p[1] };

                    if (it.get("treasure") > 0) {
                        ed[81][81] = '!';
                    }
                    if (cp[0] == 81 && cp[1] == 81 && it.get("treasure") > 0) {
                        ArrayList<Integer[]> r = new ArrayList<Integer[]>();
                        r.add(d);
                        return r;
                    }
                }
                further_path = this.explore(new Integer[] { cp[0], cp[1] }, ea, ed, it, raf, nav);
            }

            this.enable_more_stone(ea, it);

            // when everywhere has been explored and no treasure in hands on spawning point,
            // make further decision
            ArrayList<Integer[]> further_decision = this.make_decision(decision_nodes, items, d, w, ea, ed, it, raf,
                    nav);

            if (further_decision.get(0) == null) {
                decision_collection.add(further_decision);
            }

            if (further_decision.get(0) != null) {
                further_decision.add(d);
                return further_decision;
            }

            ddd = d;
        }

        // handle the uncertainty: when the agent cannot simulate the 'win' in current
        // context, it pick the farthest decision branch. the null in the beginning
        // place identifies it as an uncertain decision
        if (decision_collection.size() > 0) {
            Integer decision_selection = 0;
            for (int i = 1; i < decision_collection.size(); i++) {
                if (decision_selection == 0) {
                    decision_selection = i;
                }

                if (decision_collection.get(i).size() > decision_collection.get(decision_selection).size()) {
                    decision_selection = i;
                }
            }
            ArrayList<Integer[]> r = decision_collection.get(decision_selection);
            r.add(ddd);
            return r;
        } else {
            ArrayList<Integer[]> r = new ArrayList<Integer[]>();
            r.add(null);
            return r;
        }
    }

    // land path finding algorithm: using seed floodfill algorithm
    ArrayList<Integer[]> find_path(Integer[] current_pos, Integer[] target_pos, char[][] world_map,
            HashMap<String, Integer> items) {
        HashMap<ArrayList<Integer>, Integer[]> d = new HashMap<ArrayList<Integer>, Integer[]>();
        ArrayList<Integer[]> seed = new ArrayList<Integer[]>();
        seed.add(current_pos);
        ArrayList<ArrayList<Integer>> back_list = new ArrayList<ArrayList<Integer>>();
        ArrayList<Integer[]> path = new ArrayList<Integer[]>();
        Integer[] r = null;
        while (true) {
            if (seed.size() <= 0) {
                r = null;
                break;
            }

            Integer[] e = seed.remove(0);
            back_list.add(new ArrayList<>(Arrays.asList(e)));

            boolean b = false;
            for (int i : new Integer[] { -1, 0, 1 }) {
                if (i == 0) {
                    for (int j : new Integer[] { -1, 1 }) {
                        if (e[0] + i == target_pos[0] && e[1] + j == target_pos[1]) {
                            r = new Integer[] { e[0] + i, e[1] + j };
                            d.put(new ArrayList<>(Arrays.asList(r)), e);
                            b = true;
                            break;
                        }
                    }
                } else {
                    Integer j = 0;
                    if (e[0] + i == target_pos[0] && e[1] + j == target_pos[1]) {
                        r = new Integer[] { e[0] + i, e[1] + j };
                        d.put(new ArrayList<>(Arrays.asList(r)), e);
                        b = true;
                        break;
                    }
                }
                if (b == true) {
                    break;
                }
            }
            if (b == true) {
                break;
            }

            for (int i : new Integer[] { -1, 0, 1 }) {
                if (i == 0) {
                    for (int j : new Integer[] { -1, 1 }) {
                        if (world_map[e[0] + i][e[1] + j] == ' ' || world_map[e[0] + i][e[1] + j] == 'O'
                                || world_map[e[0] + i][e[1] + j] == 'a' || world_map[e[0] + i][e[1] + j] == 'k'
                                || world_map[e[0] + i][e[1] + j] == '$' || world_map[e[0] + i][e[1] + j] == 'o'
                                || (world_map[e[0] + i][e[1] + j] == 'T' && items.get("axe") > 0
                                        && items.get("raft") <= 0)
                                || (world_map[e[0] + i][e[1] + j] == '-' && items.get("key") > 0)) {
                            if (back_list.contains(
                                    new ArrayList<>(Arrays.asList(new Integer[] { e[0] + i, e[1] + j }))) == false) {
                                seed.add(new Integer[] { e[0] + i, e[1] + j });
                                back_list.add(new ArrayList<>(Arrays.asList(new Integer[] { e[0] + i, e[1] + j })));
                                d.put(new ArrayList<>(Arrays.asList(new Integer[] { e[0] + i, e[1] + j })), e);
                            }
                        }
                    }
                } else {
                    int j = 0;
                    if (world_map[e[0] + i][e[1] + j] == ' ' || world_map[e[0] + i][e[1] + j] == 'O'
                            || world_map[e[0] + i][e[1] + j] == 'a' || world_map[e[0] + i][e[1] + j] == 'k'
                            || world_map[e[0] + i][e[1] + j] == '$' || world_map[e[0] + i][e[1] + j] == 'o'
                            || (world_map[e[0] + i][e[1] + j] == 'T' && items.get("axe") > 0 && items.get("raft") <= 0)
                            || (world_map[e[0] + i][e[1] + j] == '-' && items.get("key") > 0)) {
                        if (back_list.contains(
                                new ArrayList<>(Arrays.asList(new Integer[] { e[0] + i, e[1] + j }))) == false) {
                            seed.add(new Integer[] { e[0] + i, e[1] + j });
                            back_list.add(new ArrayList<>(Arrays.asList(new Integer[] { e[0] + i, e[1] + j })));
                            d.put(new ArrayList<>(Arrays.asList(new Integer[] { e[0] + i, e[1] + j })), e);
                        }
                    }
                }
            }
        }

        if (r == null) {
            return new ArrayList<Integer[]>();
        }
        Integer[] pp = r;
        while (d.get(new ArrayList<>(Arrays.asList(pp))) != null) {
            path.add(pp);
            pp = d.get(new ArrayList<>(Arrays.asList(pp)));
        }
        Collections.reverse(path);
        return path;
    }

    // sea path finding algorithm: using seed floodfill algorithm
    ArrayList<Integer[]> find_water_path(Integer[] current_pos, Integer[] target_pos, char[][] explorable_map,
            HashMap<String, Integer> items, boolean[] rafting, boolean[] navigating) {
        HashMap<ArrayList<Integer>, Integer[]> d = new HashMap<ArrayList<Integer>, Integer[]>();
        ArrayList<Integer[]> seed = new ArrayList<Integer[]>();
        seed.add(current_pos);
        ArrayList<ArrayList<Integer>> back_list = new ArrayList<ArrayList<Integer>>();
        ArrayList<Integer[]> path = new ArrayList<Integer[]>();
        Integer[] r = null;
        while (true) {
            if (seed.size() <= 0) {
                r = null;
                break;
            }

            Integer[] e = seed.remove(0);
            back_list.add(new ArrayList<>(Arrays.asList(e)));
            boolean b = false;
            for (int i : new Integer[] { -1, 0, 1 }) {
                if (i == 0) {
                    for (int j : new Integer[] { -1, 1 }) {
                        if (e[0] + i == target_pos[0] && e[1] + j == target_pos[1]) {
                            r = new Integer[] { e[0] + i, e[1] + j };
                            d.put(new ArrayList<>(Arrays.asList(r)), e);
                            b = true;
                            break;
                        }
                    }
                } else {
                    Integer j = 0;
                    if (e[0] + i == target_pos[0] && e[1] + j == target_pos[1]) {
                        r = new Integer[] { e[0] + i, e[1] + j };
                        d.put(new ArrayList<>(Arrays.asList(r)), e);
                        b = true;
                        break;
                    }
                }
                if (b == true) {
                    break;
                }
            }
            if (b == true) {
                break;
            }

            for (int i : new Integer[] { -1, 0, 1 }) {
                if (i == 0) {
                    for (int j : new Integer[] { -1, 1 }) {
                        if (explorable_map[e[0] + i][e[1] + j] == '~') {
                            if (back_list.contains(
                                    new ArrayList<>(Arrays.asList(new Integer[] { e[0] + i, e[1] + j }))) == false) {
                                seed.add(new Integer[] { e[0] + i, e[1] + j });
                                back_list.add(new ArrayList<>(Arrays.asList(new Integer[] { e[0] + i, e[1] + j })));
                                d.put(new ArrayList<>(Arrays.asList(new Integer[] { e[0] + i, e[1] + j })), e);
                            }
                        }
                    }
                } else {
                    Integer j = 0;
                    if (world_map[e[0] + i][e[1] + j] == '~') {
                        if (back_list.contains(
                                new ArrayList<>(Arrays.asList(new Integer[] { e[0] + i, e[1] + j }))) == false) {
                            seed.add(new Integer[] { e[0] + i, e[1] + j });
                            back_list.add(new ArrayList<>(Arrays.asList(new Integer[] { e[0] + i, e[1] + j })));
                            d.put(new ArrayList<>(Arrays.asList(new Integer[] { e[0] + i, e[1] + j })), e);
                        }
                    }
                }
            }
        }

        if (navigating[0] == true) {
            navigating[0] = false;
            rafting[0] = false;
        }

        if (r == null) {
            return new ArrayList<Integer[]>();
        }
        Integer[] pp = r;
        while (d.get(new ArrayList<>(Arrays.asList(pp))) != null) {
            path.add(pp);
            pp = d.get(new ArrayList<>(Arrays.asList(pp)));
        }
        Collections.reverse(path);
        return path;
    }

    // navigate land when in the water
    ArrayList<Integer[]> find_coast(Integer[] current_pos, char[][] world_map, HashMap<String, Integer> items) {
        ArrayList<Integer[]> seed = new ArrayList<Integer[]>();
        seed.add(current_pos);
        ArrayList<ArrayList<Integer>> back_list = new ArrayList<ArrayList<Integer>>();
        ArrayList<Integer[]> coast = new ArrayList<Integer[]>();
        ArrayList<ArrayList<Integer>> coast_checker = new ArrayList<ArrayList<Integer>>();
        while (true) {
            if (seed.size() <= 0) {
                break;
            }

            Integer[] e = seed.remove(0);
            back_list.add(new ArrayList<>(Arrays.asList(e)));

            for (int i : new Integer[] { -1, 0, 1 }) {
                if (i == 0) {
                    for (int j : new Integer[] { -1, 1 }) {
                        if (world_map[e[0] + i][e[1] + j] == ' ' || world_map[e[0] + i][e[1] + j] == 'O'
                                || world_map[e[0] + i][e[1] + j] == 'a' || world_map[e[0] + i][e[1] + j] == 'k'
                                || world_map[e[0] + i][e[1] + j] == '$' || world_map[e[0] + i][e[1] + j] == 'o'
                                || (world_map[e[0] + i][e[1] + j] == 'T' && items.get("axe") > 0)
                                || (world_map[e[0] + i][e[1] + j] == '-' && items.get("key") > 0)) {
                            if (coast_checker.contains(
                                    new ArrayList<>(Arrays.asList(new Integer[] { e[0] + i, e[1] + j }))) == false) {
                                coast_checker.add(new ArrayList<>(Arrays.asList(new Integer[] { e[0] + i, e[1] + j })));
                            }
                        }
                    }
                } else {
                    Integer j = 0;
                    if (world_map[e[0] + i][e[1] + j] == ' ' || world_map[e[0] + i][e[1] + j] == 'O'
                            || world_map[e[0] + i][e[1] + j] == 'a' || world_map[e[0] + i][e[1] + j] == 'k'
                            || world_map[e[0] + i][e[1] + j] == '$' || world_map[e[0] + i][e[1] + j] == 'o'
                            || (world_map[e[0] + i][e[1] + j] == 'T' && items.get("axe") > 0)
                            || (world_map[e[0] + i][e[1] + j] == '-' && items.get("key") > 0)) {
                        if (coast_checker.contains(
                                new ArrayList<>(Arrays.asList(new Integer[] { e[0] + i, e[1] + j }))) == false) {
                            coast_checker.add(new ArrayList<>(Arrays.asList(new Integer[] { e[0] + i, e[1] + j })));
                        }
                    }
                }
            }

            for (int i : new Integer[] { -1, 0, 1 }) {
                if (i == 0) {
                    for (int j : new Integer[] { -1, 1 }) {
                        if (world_map[e[0] + i][e[1] + j] == '~') {
                            if (back_list.contains(
                                    new ArrayList<>(Arrays.asList(new Integer[] { e[0] + i, e[1] + j }))) == false) {
                                seed.add(new Integer[] { e[0] + i, e[1] + j });
                                back_list.add(new ArrayList<>(Arrays.asList(new Integer[] { e[0] + i, e[1] + j })));
                            }
                        }
                    }
                } else {
                    Integer j = 0;
                    if (world_map[e[0] + i][e[1] + j] == '~') {
                        if (back_list.contains(
                                new ArrayList<>(Arrays.asList(new Integer[] { e[0] + i, e[1] + j }))) == false) {
                            seed.add(new Integer[] { e[0] + i, e[1] + j });
                            back_list.add(new ArrayList<>(Arrays.asList(new Integer[] { e[0] + i, e[1] + j })));
                        }
                    }
                }
            }
        }

        ArrayList<ArrayList<Integer>> tree_checker = new ArrayList<ArrayList<Integer>>();
        for (ArrayList<Integer> ar : coast_checker) {
            if (world_map[ar.get(0)][ar.get(1)] == 'T') {
                tree_checker.add(ar);
            } else {
                coast.add(ar.toArray(new Integer[2]));
            }
        }

        for (ArrayList<Integer> ar : tree_checker) {
            coast.add(ar.toArray(new Integer[2]));
        }

        return coast;
    }

    // navigate sea when on land
    ArrayList<Integer[]> find_seaside(Integer[] current_pos, char[][] world_map, HashMap<String, Integer> items) {
        ArrayList<Integer[]> seed = new ArrayList<Integer[]>();
        seed.add(current_pos);
        ArrayList<ArrayList<Integer>> back_list = new ArrayList<ArrayList<Integer>>();
        ArrayList<Integer[]> seaside = new ArrayList<Integer[]>();
        ArrayList<ArrayList<Integer>> seaside_checker = new ArrayList<ArrayList<Integer>>();
        while (true) {
            if (seed.size() <= 0) {
                break;
            }

            Integer[] e = seed.remove(0);
            back_list.add(new ArrayList<>(Arrays.asList(e)));

            for (Integer i : new Integer[] { -1, 0, 1 }) {
                if (i == 0) {
                    for (Integer j : new Integer[] { -1, 1 }) {
                        if (world_map[e[0] + i][e[1] + j] == '~' && (items.get("stone") > 0 || items.get("raft") > 0)) {
                            if (seaside_checker.contains(
                                    new ArrayList<>(Arrays.asList(new Integer[] { e[0] + i, e[1] + j }))) == false) {
                                seaside_checker
                                        .add(new ArrayList<>(Arrays.asList(new Integer[] { e[0] + i, e[1] + j })));
                            }
                        }
                    }
                } else {
                    Integer j = 0;
                    if (world_map[e[0] + i][e[1] + j] == '~' && (items.get("stone") > 0 || items.get("raft") > 0)) {
                        if (seaside_checker.contains(
                                new ArrayList<>(Arrays.asList(new Integer[] { e[0] + i, e[1] + j }))) == false) {
                            seaside_checker.add(new ArrayList<>(Arrays.asList(new Integer[] { e[0] + i, e[1] + j })));
                        }
                    }
                }
            }

            for (Integer i : new Integer[] { -1, 0, 1 }) {
                if (i == 0) {
                    for (Integer j : new Integer[] { -1, 1 }) {
                        if (world_map[e[0] + i][e[1] + j] == ' ' || world_map[e[0] + i][e[1] + j] == 'O') {
                            if (back_list.contains(
                                    new ArrayList<>(Arrays.asList(new Integer[] { e[0] + i, e[1] + j }))) == false) {
                                seed.add(new Integer[] { e[0] + i, e[1] + j });
                                back_list.add(new ArrayList<>(Arrays.asList(new Integer[] { e[0] + i, e[1] + j })));
                            }
                        }
                    }
                } else {
                    Integer j = 0;
                    if (world_map[e[0] + i][e[1] + j] == ' ' || world_map[e[0] + i][e[1] + j] == 'O') {
                        if (back_list.contains(
                                new ArrayList<>(Arrays.asList(new Integer[] { e[0] + i, e[1] + j }))) == false) {
                            seed.add(new Integer[] { e[0] + i, e[1] + j });
                            back_list.add(new ArrayList<>(Arrays.asList(new Integer[] { e[0] + i, e[1] + j })));
                        }
                    }
                }
            }
        }

        for (ArrayList<Integer> ar : seaside_checker) {
            seaside.add(ar.toArray(new Integer[2]));
        }

        return seaside;
    }

    // pruning: merge the duplicated outbound points
    boolean merge_seaside_choices(Integer[] p1, Integer[] p2, char[][] world_map) {
        ArrayList<Integer[]> seed = new ArrayList<Integer[]>();
        seed.add(p1);
        ArrayList<ArrayList<Integer>> back_list = new ArrayList<ArrayList<Integer>>();
        while (true) {
            if (seed.size() <= 0) {
                break;
            }

            Integer[] e = seed.remove(0);
            back_list.add(new ArrayList<>(Arrays.asList(e)));

            for (int i : new Integer[] { -1, 0, 1 }) {
                if (i == 0) {
                    for (int j : new Integer[] { -1, 1 }) {
                        if (e[0] + i == p2[0] && e[1] + j == p2[1]) {
                            return true;
                        }
                    }
                } else {
                    Integer j = 0;
                    if (e[0] + i == p2[0] && e[1] + j == p2[1]) {
                        return true;
                    }
                }
            }

            for (int i : new Integer[] { -1, 0, 1 }) {
                if (i == 0) {
                    for (int j : new Integer[] { -1, 1 }) {
                        if (world_map[e[0] + i][e[1] + j] == '~') {
                            if (back_list.contains(
                                    new ArrayList<>(Arrays.asList(new Integer[] { e[0] + i, e[1] + j }))) == false) {
                                seed.add(new Integer[] { e[0] + i, e[1] + j });
                                back_list.add(new ArrayList<>(Arrays.asList(new Integer[] { e[0] + i, e[1] + j })));
                            }
                        }
                    }
                } else {
                    Integer j = 0;
                    if (world_map[e[0] + i][e[1] + j] == '~') {
                        if (back_list.contains(
                                new ArrayList<>(Arrays.asList(new Integer[] { e[0] + i, e[1] + j }))) == false) {
                            seed.add(new Integer[] { e[0] + i, e[1] + j });
                            back_list.add(new ArrayList<>(Arrays.asList(new Integer[] { e[0] + i, e[1] + j })));
                        }
                    }
                }
            }
        }
        return false;
    }

    // pruning: merge the duplicated inbound points
    boolean merge_coast_choices(Integer[] p1, Integer[] p2, char[][] world_map) {
        ArrayList<Integer[]> seed = new ArrayList<Integer[]>();
        seed.add(p1);
        ArrayList<ArrayList<Integer>> back_list = new ArrayList<ArrayList<Integer>>();
        while (true) {
            if (seed.size() <= 0) {
                break;
            }

            Integer[] e = seed.remove(0);
            back_list.add(new ArrayList<>(Arrays.asList(e)));

            for (int i : new Integer[] { -1, 0, 1 }) {
                if (i == 0) {
                    for (int j : new Integer[] { -1, 1 }) {
                        if (e[0] + i == p2[0] && e[1] + j == p2[1]) {
                            return true;
                        }
                    }
                } else {
                    Integer j = 0;
                    if (e[0] + i == p2[0] && e[1] + j == p2[1]) {
                        return true;
                    }
                }
            }

            for (int i : new Integer[] { -1, 0, 1 }) {
                if (i == 0) {
                    for (int j : new Integer[] { -1, 1 }) {
                        if (world_map[e[0] + i][e[1] + j] == ' ') {
                            if (back_list.contains(
                                    new ArrayList<>(Arrays.asList(new Integer[] { e[0] + i, e[1] + j }))) == false) {
                                seed.add(new Integer[] { e[0] + i, e[1] + j });
                                back_list.add(new ArrayList<>(Arrays.asList(new Integer[] { e[0] + i, e[1] + j })));
                            }
                        }
                    }
                } else {
                    Integer j = 0;
                    if (world_map[e[0] + i][e[1] + j] == ' ') {
                        if (back_list.contains(
                                new ArrayList<>(Arrays.asList(new Integer[] { e[0] + i, e[1] + j }))) == false) {
                            seed.add(new Integer[] { e[0] + i, e[1] + j });
                            back_list.add(new ArrayList<>(Arrays.asList(new Integer[] { e[0] + i, e[1] + j })));
                        }
                    }
                }
            }
        }
        return false;
    }

    // pruning: remove unreachable stone steps
    ArrayList<Integer[]> remain_useful_water(List<Integer[]> decisions, char[][] world_map, char[][] explored_map) {
        ArrayList<Integer[]> ret = new ArrayList<Integer[]>();
        for (Integer[] d : decisions) {
            ArrayList<Integer[]> seed = new ArrayList<Integer[]>();
            seed.add(d);
            ArrayList<ArrayList<Integer>> back_list = new ArrayList<ArrayList<Integer>>();
            while (true) {
                if (seed.size() <= 0) {
                    break;
                }

                Integer[] e = seed.remove(0);
                back_list.add(new ArrayList<>(Arrays.asList(3)));

                boolean b = false;
                for (int i : new Integer[] { -1, 0, 1 }) {
                    if (i == 0) {
                        for (int j : new Integer[] { -1, 1 }) {
                            if (explored_map[e[0] + i][e[1] + j] == '!' && world_map[e[0] + i][e[1] + j] != '~'
                                    && world_map[e[0] + i][e[1] + j] != 'X' && world_map[e[0] + i][e[1] + j] != '*'
                                    && world_map[e[0] + i][e[1] + j] != '.') {
                                ret.add(d);
                                b = true;
                                break;
                            }
                        }
                    } else {
                        Integer j = 0;
                        if (explored_map[e[0] + i][e[1] + j] == '!' && world_map[e[0] + i][e[1] + j] != '~'
                                && world_map[e[0] + i][e[1] + j] != 'X' && world_map[e[0] + i][e[1] + j] != '*'
                                && world_map[e[0] + i][e[1] + j] != '.') {
                            ret.add(d);
                            b = true;
                            break;
                        }
                    }
                    if (b == true) {
                        break;
                    }
                }

                if (b == true) {
                    break;
                }

                for (int i : new Integer[] { -1, 0, 1 }) {
                    if (i == 0) {
                        for (int j : new Integer[] { -1, 1 }) {
                            if (world_map[e[0] + i][e[1] + j] == '~') {
                                if (back_list.contains(new ArrayList<>(
                                        Arrays.asList(new Integer[] { e[0] + i, e[1] + j }))) == false) {
                                    seed.add(new Integer[] { e[0] + i, e[1] + j });
                                    back_list.add(new ArrayList<>(Arrays.asList(new Integer[] { e[0] + i, e[1] + j })));
                                }
                            }
                        }
                    } else {
                        Integer j = 0;
                        if (world_map[e[0] + i][e[1] + j] == '~') {
                            if (back_list.contains(
                                    new ArrayList<>(Arrays.asList(new Integer[] { e[0] + i, e[1] + j }))) == false) {
                                seed.add(new Integer[] { e[0] + i, e[1] + j });
                                back_list.add(new ArrayList<>(Arrays.asList(new Integer[] { e[0] + i, e[1] + j })));
                            }
                        }
                    }
                }
            }
        }
        return ret;
    }

    // explore
    ArrayList<Integer[]> explore(Integer[] current_pos, char[][] explorable_map, char[][] explored_map,
            HashMap<String, Integer> items, boolean[] rafting, boolean[] navigating) {
        if (rafting[0] == false) {
            return this.find_nearest_explorable(current_pos, explorable_map, explored_map, items);
        } else {
            return this.find_nearest_explorable_waterway(current_pos, explorable_map, explored_map, items, rafting,
                    navigating);
        }
    }

    // land explore algorithm: using seed floodfill algorithm
    ArrayList<Integer[]> find_nearest_explorable(Integer[] current_pos, char[][] explorable_map, char[][] explored_map,
            HashMap<String, Integer> items) {
        HashMap<ArrayList<Integer>, Integer[]> d = new HashMap<ArrayList<Integer>, Integer[]>();
        ArrayList<Integer[]> seed = new ArrayList<Integer[]>();
        seed.add(current_pos);
        ArrayList<ArrayList<Integer>> back_list = new ArrayList<ArrayList<Integer>>();
        ArrayList<Integer[]> path = new ArrayList<Integer[]>();
        Integer[] r = null;
        while (true) {
            if (seed.size() <= 0) {
                r = null;
                break;
            }
            Integer[] e = seed.remove(0);
            back_list.add(new ArrayList<>(Arrays.asList(e)));
            boolean b = false;
            for (int i : new Integer[] { -1, 0, 1 }) {
                if (i == 0) {
                    for (int j : new Integer[] { -1, 1 }) {
                        if (explored_map[e[0] + i][e[1] + j] == '!'
                                && (explorable_map[e[0] + i][e[1] + j] == '@'
                                        || (explorable_map[e[0] + i][e[1] + j] == 'T' && items.get("axe") > 0
                                                && items.get("raft") <= 0)
                                        || (explorable_map[e[0] + i][e[1] + j] == '-' && items.get("key") > 0))
                                || (explorable_map[e[0] + i][e[1] + j] == '&' && items.get("stone") <= 0)) {
                            r = new Integer[] { e[0] + i, e[1] + j };
                            d.put(new ArrayList<>(Arrays.asList(r)), e);
                            b = true;
                            break;
                        }
                    }
                } else {
                    Integer j = 0;
                    if (explored_map[e[0] + i][e[1] + j] == '!'
                            && (explorable_map[e[0] + i][e[1] + j] == '@'
                                    || (explorable_map[e[0] + i][e[1] + j] == 'T' && items.get("axe") > 0
                                            && items.get("raft") <= 0)
                                    || (explorable_map[e[0] + i][e[1] + j] == '-' && items.get("key") > 0))
                            || (explorable_map[e[0] + i][e[1] + j] == '&' && items.get("stone") <= 0)) {
                        r = new Integer[] { e[0] + i, e[1] + j };
                        d.put(new ArrayList<>(Arrays.asList(r)), e);
                        b = true;
                        break;
                    }
                }
                if (b == true) {
                    break;
                }
            }
            if (b == true) {
                break;
            }

            for (int i : new Integer[] { -1, 0, 1 }) {
                if (i == 0) {
                    for (int j : new Integer[] { -1, 1 }) {
                        if (explored_map[e[0] + i][e[1] + j] == '+' && explorable_map[e[0] + i][e[1] + j] == '@') {
                            if (back_list.contains(
                                    new ArrayList<>(Arrays.asList(new Integer[] { e[0] + i, e[1] + j }))) == false) {
                                seed.add(new Integer[] { e[0] + i, e[1] + j });
                                back_list.add(new ArrayList<>(Arrays.asList(new Integer[] { e[0] + i, e[1] + j })));
                                d.put(new ArrayList<>(Arrays.asList(new Integer[] { e[0] + i, e[1] + j })), e);
                            }
                        }
                    }
                } else {
                    Integer j = 0;
                    if (explored_map[e[0] + i][e[1] + j] == '+' && explorable_map[e[0] + i][e[1] + j] == '@') {
                        if (back_list.contains(
                                new ArrayList<>(Arrays.asList(new Integer[] { e[0] + i, e[1] + j }))) == false) {
                            seed.add(new Integer[] { e[0] + i, e[1] + j });
                            back_list.add(new ArrayList<>(Arrays.asList(new Integer[] { e[0] + i, e[1] + j })));
                            d.put(new ArrayList<>(Arrays.asList(new Integer[] { e[0] + i, e[1] + j })), e);
                        }
                    }
                }
            }
        }

        if (r == null) {
            return new ArrayList<Integer[]>();
        }
        Integer[] pp = r;
        while (d.get(new ArrayList<>(Arrays.asList(pp))) != null) {
            path.add(pp);
            pp = d.get(new ArrayList<>(Arrays.asList(pp)));
        }
        Collections.reverse(path);
        return path;
    }

    // sea explore algorithm: using seed floodfill algorithm
    ArrayList<Integer[]> find_nearest_explorable_waterway(Integer[] current_pos, char[][] explorable_map,
            char[][] explored_map, HashMap<String, Integer> items, boolean[] rafting, boolean[] navigating) {
        HashMap<ArrayList<Integer>, Integer[]> d = new HashMap<ArrayList<Integer>, Integer[]>();
        ArrayList<Integer[]> seed = new ArrayList<Integer[]>();
        seed.add(current_pos);
        ArrayList<ArrayList<Integer>> back_list = new ArrayList<ArrayList<Integer>>();
        ArrayList<Integer[]> path = new ArrayList<Integer[]>();
        Integer[] r = null;
        while (true) {
            if (seed.size() <= 0) {
                r = null;
                break;
            }

            Integer[] e = seed.remove(0);
            back_list.add(new ArrayList<>(Arrays.asList(e)));
            boolean b = false;
            if (navigating[0] == true) {
                for (int i : new Integer[] { -1, 0, 1 }) {
                    if (i == 0) {
                        for (int j : new Integer[] { -1, 1 }) {
                            if (explorable_map[e[0] + i][e[1] + j] == '=') {
                                r = new Integer[] { e[0] + i, e[1] + j };
                                d.put(new ArrayList<>(Arrays.asList(r)), e);
                                b = true;
                                break;
                            }
                        }
                    } else {
                        Integer j = 0;
                        if (explorable_map[e[0] + i][e[1] + j] == '=') {
                            r = new Integer[] { e[0] + i, e[1] + j };
                            d.put(new ArrayList<>(Arrays.asList(r)), e);
                            b = true;
                            break;
                        }
                    }
                    if (b == true) {
                        break;
                    }
                }
                if (b == true) {
                    break;
                }
            } else {
                for (int i : new Integer[] { -1, 0, 1 }) {
                    if (i == 0) {
                        for (int j : new Integer[] { -1, 1 }) {
                            if (explored_map[e[0] + i][e[1] + j] == '!' && explorable_map[e[0] + i][e[1] + j] == '~') {
                                r = new Integer[] { e[0] + i, e[1] + j };
                                d.put(new ArrayList<>(Arrays.asList(r)), e);
                                b = true;
                                break;
                            }
                        }
                    } else {
                        Integer j = 0;
                        if (explored_map[e[0] + i][e[1] + j] == '!' && explorable_map[e[0] + i][e[1] + j] == '~') {
                            r = new Integer[] { e[0] + i, e[1] + j };
                            d.put(new ArrayList<>(Arrays.asList(r)), e);
                            b = true;
                            break;
                        }
                    }
                    if (b == true) {
                        break;
                    }
                }
                if (b == true) {
                    break;
                }
            }

            for (int i : new Integer[] { -1, 0, 1 }) {
                if (i == 0) {
                    for (int j : new Integer[] { -1, 1 }) {
                        if (explored_map[e[0] + i][e[1] + j] == '+' && explorable_map[e[0] + i][e[1] + j] == '~') {
                            if (back_list.contains(
                                    new ArrayList<>(Arrays.asList(new Integer[] { e[0] + i, e[1] + j }))) == false) {
                                seed.add(new Integer[] { e[0] + i, e[1] + j });
                                back_list.add(new ArrayList<>(Arrays.asList(new Integer[] { e[0] + i, e[1] + j })));
                                d.put(new ArrayList<>(Arrays.asList(new Integer[] { e[0] + i, e[1] + j })), e);
                            }
                        }
                    }
                } else {
                    Integer j = 0;
                    if (explored_map[e[0] + i][e[1] + j] == '+' && explorable_map[e[0] + i][e[1] + j] == '~') {
                        if (back_list.contains(
                                new ArrayList<>(Arrays.asList(new Integer[] { e[0] + i, e[1] + j }))) == false) {
                            seed.add(new Integer[] { e[0] + i, e[1] + j });
                            back_list.add(new ArrayList<>(Arrays.asList(new Integer[] { e[0] + i, e[1] + j })));
                            d.put(new ArrayList<>(Arrays.asList(new Integer[] { e[0] + i, e[1] + j })), e);
                        }
                    }
                }
            }
        }

        if (navigating[0] == true) {
            navigating[0] = false;
            rafting[0] = false;
        }

        if (r == null) {
            return new ArrayList<Integer[]>();
        }
        Integer[] pp = r;
        while (d.get(new ArrayList<>(Arrays.asList(pp))) != null) {
            path.add(pp);
            pp = d.get(new ArrayList<>(Arrays.asList(pp)));
        }
        Collections.reverse(path);
        return path;
    }

    // generate 'micro' actions from the path
    char choose_action(char[][] world_map, ArrayList<Integer[]> path, ArrayList<Character> act_list,
            Integer[] current_pos) {
        if (act_list.size() > 0) {
            return act_list.remove(0);
        }

        if (direction == 0) {
            if (path.get(0)[0] == current_pos[0] && path.get(0)[1] == current_pos[1] - 1) {
                act_list.add('l');
            }
            if (path.get(0)[0] == current_pos[0] + 1 && path.get(0)[1] == current_pos[1]) {
                act_list.add('l');
                act_list.add('l');
            }
            if (path.get(0)[0] == current_pos[0] && path.get(0)[1] == current_pos[1] + 1) {
                act_list.add('r');
            }
        }
        if (direction == 1) {
            if (path.get(0)[0] == current_pos[0] - 1 && path.get(0)[1] == current_pos[1]) {
                act_list.add('l');
            }
            if (path.get(0)[0] == current_pos[0] && path.get(0)[1] == current_pos[1] - 1) {
                act_list.add('l');
                act_list.add('l');
            }
            if (path.get(0)[0] == current_pos[0] + 1 && path.get(0)[1] == current_pos[1]) {
                act_list.add('r');
            }
        }
        if (direction == 2) {
            if (path.get(0)[0] == current_pos[0] && path.get(0)[1] == current_pos[1] + 1) {
                act_list.add('l');
            }
            if (path.get(0)[0] == current_pos[0] - 1 && path.get(0)[1] == current_pos[1]) {
                act_list.add('l');
                act_list.add('l');
            }
            if (path.get(0)[0] == current_pos[0] && path.get(0)[1] == current_pos[1] - 1) {
                act_list.add('r');
            }
        }
        if (direction == 3) {
            if (path.get(0)[0] == current_pos[0] + 1 && path.get(0)[1] == current_pos[1]) {
                act_list.add('l');
            }
            if (path.get(0)[0] == current_pos[0] && path.get(0)[1] == current_pos[1] + 1) {
                act_list.add('l');
                act_list.add('l');
            }
            if (path.get(0)[0] == current_pos[0] - 1 && path.get(0)[1] == current_pos[1]) {
                act_list.add('r');
            }
        }

        if (world_map[path.get(0)[0]][path.get(0)[1]] == '-') {
            act_list.add('u');
        }
        if (world_map[path.get(0)[0]][path.get(0)[1]] == 'T') {
            act_list.add('c');
        }

        act_list.add('f');
        path.remove(0);
        return act_list.remove(0);
    }

    // the method to handle the query
    public char get_action(char view[][]) {
        this.refresh_stones(this.explorable_map, this.items);

        this.direction = this.get_direction(this.direction, this.last_input);

        this.current_pos = get_pos(this.current_pos, this.direction, this.last_input);

        this.get_world_map(view, this.world_map, this.explorable_map, this.explored_map, this.current_pos,
                this.direction, this.last_input);
        if (this.navigating[0] == true) {
            this.rafting[0] = false;
            this.navigating[0] = false;
        }

        if (this.path.size() <= 0 && this.act_list.size() <= 0) {
            this.path = explore(this.current_pos, this.explorable_map, this.explored_map, this.items, this.rafting,
                    this.navigating);
        }

        if (this.path.size() <= 0 && this.act_list.size() <= 0) {
            if (this.decisions.size() <= 0) {
                this.enable_more_stone(this.explorable_map, this.items);
                this.decisions = this.make_decision(new ArrayList<Integer[]>(), this.items, this.current_pos,
                        this.world_map, this.explorable_map, this.explored_map, this.items, this.rafting,
                        this.navigating);

                for (int i = 0; i < explorable_map.length; i++) {
                    for (int j = 0; j < this.explorable_map[i].length; j++) {
                        this.explorable_map[i][j] = '#';
                    }
                }

                for (int i = 0; i < explored_map.length; i++) {
                    for (int j = 0; j < this.explored_map[i].length; j++) {
                        this.explored_map[i][j] = '!';
                    }
                }

                this.explorable_map[this.current_pos[0]][this.current_pos[1]] = '@';
                this.explored_map[this.current_pos[0]][this.current_pos[1]] = '+';

                if (this.items.get("treasure") > 0) {
                    this.explored_map[81][81] = '!';
                }
            }

            if (this.decisions.size() > 0) {
                Integer tempd[] = null;
                if (this.decisions.get(0) == null) {
                    this.decisions.remove(0);
                    if (this.decisions.size() > 0) {
                        tempd = this.decisions.remove(this.decisions.size() - 1);
                        this.decisions = new ArrayList<Integer[]>();
                    }
                } else {
                    tempd = this.decisions.remove(this.decisions.size() - 1);
                }
                if (tempd != null) {
                    if (this.rafting[0] == true) {
                        if (this.world_map[tempd[0]][tempd[1]] == 'T') {
                            ArrayList<Integer[]> coast = this.find_coast(this.current_pos, this.world_map, this.items);
                            for (Integer[] cc : coast) {
                                if (this.world_map[cc[0]][cc[1]] != 'T' && this.world_map[cc[0]][cc[1]] != '~'
                                        && this.world_map[cc[0]][cc[1]] != '*' && this.world_map[cc[0]][cc[1]] != '.'
                                        && this.world_map[cc[0]][cc[1]] != 'X') {
                                    if (merge_coast_choices(new Integer[] { cc[0], cc[1] }, tempd, this.world_map)) {
                                        tempd[0] = cc[0];
                                        tempd[1] = cc[1];
                                        break;
                                    }
                                }
                            }
                        }
                        this.path = this.find_water_path(this.current_pos, tempd, this.world_map, this.items,
                                this.rafting, this.navigating);
                    } else {
                        this.path = this.find_path(this.current_pos, tempd, this.world_map, this.items);
                    }
                }
            }
        }

        if (this.path.size() > 0) {
            if (this.rafting[0] == false && this.items.get("stone") <= 0
                    && this.world_map[this.path.get(0)[0]][this.path.get(0)[1]] == '~') {
                this.rafting[0] = true;
                this.explorable_map[this.path.get(this.path.size() - 1)[0]][this.path
                        .get(this.path.size() - 1)[1]] = '=';
            }
            if (this.rafting[0] == true && this.world_map[this.path.get(0)[0]][this.path.get(0)[1]] != '~') {
                this.navigating[0] = true;
                this.rafting[0] = true;
                this.explorable_map[this.path.get(this.path.size() - 1)[0]][this.path
                        .get(this.path.size() - 1)[1]] = '=';
            }
        }

        if (this.items.get("treasure") > 0) {
            this.explored_map[81][81] = '!';
        }

        char action = this.choose_action(this.world_map, this.path, this.act_list, this.current_pos);

        this.last_input = action;

        this.get_items(this.items, view, this.world_map, this.current_pos, this.direction, this.last_input);

        if (this.items.get("treasure") > 0) {
            this.explored_map[81][81] = '!';
        }

        return action;
    }

    // print maps for debug use
    void print_map(char map[][], Integer[] arr) {
        System.out.print("+-----+\n");
        for (int ln = 0; ln < map.length; ln++) {
            System.out.print('|');
            for (int p = 0; p < map[ln].length; p++) {
                if (p == arr[1] && ln == arr[0]) {
                    System.out.print('?');
                } else {
                    System.out.print(map[ln][p]);
                }

            }
            System.out.print('|');
            System.out.print('\n');
        }
        System.out.print("+-----+\n");
    }

    // print items for debug use
    void print_items(HashMap<String, Integer> items) {
        for (String k : items.keySet()) {
            System.out.print(k);
            System.out.print(' ');
            System.out.print(items.get(k));
            System.out.print('\n');
        }
    }

    // print views for debug use
    void print_view(char view[][]) {
        Integer i, j;

        System.out.println("\n+-----+");
        for (i = 0; i < 5; i++) {
            System.out.print("|");
            for (j = 0; j < 5; j++) {
                if ((i == 2 && j == 2)) {
                    System.out.print('^');
                } else {
                    System.out.print(view[i][j]);
                }
            }
            System.out.println("|");
        }
        System.out.println("+-----+");
    }

    public static void main(String[] args) {
        InputStream in = null;
        OutputStream out = null;
        Socket socket = null;
        Agent agent = new Agent();
        char view[][] = new char[5][5];
        char action = ' ';
        int port;
        int ch;
        int i, j;

        if (args.length < 2) {
            System.out.println("Usage: java Agent -p <port>\n");
            System.exit(-1);
        }

        port = Integer.parseInt(args[1]);

        try {
            socket = new Socket("localhost", port);
            in = socket.getInputStream();
            out = socket.getOutputStream();
        } catch (IOException e) {
            System.out.println("Could not bind to port: " + port);
            System.exit(-1);
        }

        try {
            while (true) {
                for (i = 0; i < 5; i++) {
                    for (j = 0; j < 5; j++) {
                        if (!(i == 2 && j == 2)) {
                            ch = in.read();
                            if (ch == -1) {
                                System.exit(-1);
                            }
                            view[i][j] = (char) ch;
                        }
                    }
                }
                action = agent.get_action(view);
                out.write(action);
            }
        } catch (IOException e) {
            System.out.println("Lost connection to port: " + port);
            System.exit(-1);
        } finally {
            try {
                socket.close();
            } catch (IOException e) {

            }
        }
    }
}
